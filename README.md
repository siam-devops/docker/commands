# List of commands that we use mostly in Docker


# Docker Info
This will give the whole detail about docker's local environment 
    docker info 

# See a list of all running containers
    docker ps                          

# See a list of all containers, even the ones not running
    docker ps -a  

# Show all images on this machine
    docker images -a

# Gracefully stop the specified container       
    docker stop <hash>

# Force shutdown of the specified container        
    docker kill <hash>

# Remove the specified container from this machine
    docker rm <hash>

# Remove all containers from this machine
    docker rm $(docker ps -a -q)           

# Remove the specified image from this machine
    docker rmi <imagename>

# Remove all images from this machine
    docker rmi $(docker images -q)

# To know the details about container we can inspect it 
    docker inspect <container-id>

# Search
Search the Docker Hub for images
    docker search centos

# Run docker image and login to the docker container and install git on it
    docker run -it centos bin/bash
    yum install git

# Create a new image from a container
For creating custom images without creating dockerfile, we can run a container and install all necessary software and exit from it. By using commit you can create a new image for you custom container.
    
    docker commit -m "added git" -a "SiamTeam" ContainerID repo_prefix/centos-git

# Executing a script or command without entering to it
You should run a container in detached mode, before issueing exec command. Or you have to start the container

    docker exec -it ContainerID ls

# Create image using current directory's Dockerfile
    
    docker build -t custom_image_name . 

    docker run -p 4000:80 container_name # Run "friendlyname" mapping port 4000 to 80
    docker run -d -p 4000:80 friendlyname         # Same thing, but in detached mode

# To export the container
    docker export container1 > container1.tar 

# To import the container
    docker import container1.tar container1:tag1

# To login to docker hub
    docker login --username=dockerid

# Tagging your image and push to Docker hub
    docker tag imgID youruserid/imagename
    docker push youruserid/imagename

# To run a httpd server and serve static resource through volume
    docker run -dit --name siam-web -p 8080:80 -v /home/siam/sites/:/usr/local/apache2/htdocs/ httpd:2.4

# Composer run
    docker-compose up # docker-compose.yml file should be in current directory scope

# To view current compose list
    docker-compose ps

# To down docker compose
    docker-compose down